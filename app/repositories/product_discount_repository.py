from fastapi import Depends
from app.db.db import get_db
from app.models.models import ProductDiscount
from .base_repository import BaseRepository
from sqlalchemy.orm import Session


class ProductDiscountRepository(BaseRepository):
    def __init__(self, session: Session = Depends(get_db)):
        super().__init__(session, ProductDiscount)

    def get_by_product(self, product_id):
        return self.query().filter_by(product_id=product_id).first()
