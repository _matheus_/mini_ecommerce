import datetime
from fastapi import Depends
from sqlalchemy.orm import Session

from app.db.db import get_db
from app.models.models import Order, OrderProduct, OrderStatus
from .base_repository import BaseRepository


class OrderRepository(BaseRepository):
    def __init__(self, session: Session = Depends(get_db)):
        super().__init__(session, Order)

    def insert_order_product(self, order_id, quantity, product_id):
        self.session.add(OrderProduct(order_id=order_id,
                         quantity=quantity, product_id=product_id))

        self.session.commit()

    def set_order_status(self, order_id, status):
        order = self.get_by_id(order_id)
        order.status = status
        self.session.add(OrderStatus(order_id=order_id,
                         status=status, created_at=datetime.datetime.now()))
        self.session.commit()
