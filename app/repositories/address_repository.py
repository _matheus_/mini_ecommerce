from fastapi.param_functions import Depends
from app.db.db import get_db
from app.models.models import Address
from .base_repository import BaseRepository
from sqlalchemy.orm import Session


class AddressRepository(BaseRepository):
    def __init__(self, session: Session = Depends(get_db)):
        super().__init__(session, Address)

    def remove_primary(self, customer_id):
        self.query().filter_by(
            customer_id=customer_id).update({'primary': False})
