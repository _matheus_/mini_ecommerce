class PaymentMethodsNotAvailableException(Exception):
    def __init__(self):
        self.message = 'This payment method is not available'
        super().__init__(self.message)


class PaymentMethodDiscountAlreadyExistsException(Exception):
    def __init__(self):
        self.message = 'Already exists a discount with this payment method'
        super().__init__(self.message)


class CouponCodeAlreadyExistsException(Exception):
    def __init__(self):
        self.message = 'This code is already been used'
        super().__init__(self.message)


class EmailAlreadyInUse(Exception):
    def __init__(self):
        self.message = 'This email is already been used'
        super().__init__(self.message)
