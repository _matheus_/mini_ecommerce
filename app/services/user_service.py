from fastapi import Depends
from app.api.customer.schemas import CustomerUserSchema
from app.common.exceptions import EmailAlreadyInUse
from app.models.models import User
from app.repositories.user_repository import UserRepository
from app.api.admin_user.schemas import AdminUserSchema, UpadateAdminUserSchema
import bcrypt


class UserService:
    def __init__(self, repository: UserRepository = Depends()):
        self.repository = repository

    def create_admin(self, user: User):
        return self._create_user(user, 'admin')

    def create_customer(self, user: User):
        return self._create_user(user, 'customer')

    def _create_user(self, user: User, role):
        self._validate_email(user.email)
        user.role = role
        user.password = self._hash_password(user.password)
        return self.repository.create(user)

    def update_user(self, id, user: dict):
        self._validate_email(user['email'], id)

        if user['password']:
            user['password'] = self._hash_password(user['password'])

        user_data = {key: value for (
            key, value) in user if value != None}

        return self.repository.update(id, user_data)

    def _validate_email(self, email, id_ignored=None):
        user = self.repository.find_by_email(email)
        if user and id_ignored != user.id:
            raise EmailAlreadyInUse()

    def _hash_password(self, password):
        return bcrypt.hashpw(password.encode('utf8'), bcrypt.gensalt())
