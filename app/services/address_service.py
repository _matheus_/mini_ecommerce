from fastapi import Depends
from app.models.models import Address
from app.repositories.address_repository import AddressRepository
from app.api.address.schemas import AddressSchema


class AddressService:
    def __init__(self, repository: AddressRepository = Depends()):
        self.repository = repository

    def create_address(self, address: AddressSchema):
        if address.primary:
            self.repository.remove_primary(address.customer_id)

        return self.repository.create(Address(**address.dict()))

    def update_address(self, id, address: AddressSchema):
        if address.primary:
            self.repository.remove_primary(address.customer_id)

        return self.repository.update(id, address.dict())
