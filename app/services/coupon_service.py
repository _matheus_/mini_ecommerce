from fastapi import Depends
from app.api.coupon.schemas import CouponSchema
from app.common.exceptions import CouponCodeAlreadyExistsException
from app.models.models import Coupon
from app.repositories.coupon_repository import CouponRepository


class CouponService:
    def __init__(self, coupon_repository: CouponRepository = Depends()):
        self.coupon_repository = coupon_repository

    def create(self, coupon: CouponSchema):
        find_by_code = self.coupon_repository.find_by_code(coupon.code)
        if find_by_code:
            raise CouponCodeAlreadyExistsException()

        self.coupon_repository.create(Coupon(**coupon.dict()))
