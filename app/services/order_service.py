import random
import string
import datetime
from typing import List
from fastapi import Depends
from app.models.models import Order
from app.repositories.product_repository import ProductRepository
from app.repositories.coupon_repository import CouponRepository
from app.repositories.product_discount_repository import ProductDiscountRepository
from app.repositories.order_repository import OrderRepository
from app.api.order.schemas import OrderSchema, ProductSchema


class OrderService:
    def __init__(self, product_repository: ProductRepository = Depends(),
                 coupon_repository: CouponRepository = Depends(),
                 product_discount_repository: ProductDiscountRepository = Depends(),
                 order_repository: OrderRepository = Depends()):
        self.product_repository = product_repository
        self.coupon_repository = coupon_repository
        self.product_discount_repository = product_discount_repository
        self.order_repository = order_repository

    def create_order(self, customer_id, order: OrderSchema):
        total_value = self._calculate_total_order(order.products)
        total_discount = self._calculate_discount(
            order.products, total_value, order.payment_method_id, order.coupon_code)

        order_data = {
            'number': self._generate_code(),
            'customer_id': customer_id,
            'address_id': order.address_id,
            'total_value': total_value,
            'payment_method_id': order.payment_method_id,
            'total_discount': total_discount,
            'created_at': datetime.datetime.now()
        }

        order_model = self.order_repository.create(Order(**order_data))
        for p in order.products:
            self.order_repository.insert_order_product(
                order_model.id, p.quantity, p.id)

        self.order_repository.set_order_status(order_model.id, 'ORDER PLACED')

    def _calculate_total_order(self, products=List[ProductSchema]):
        total = 0
        for p in products:
            product = self.product_repository.get_by_id(p.id)
            if not product:
                raise Exception(f'Product with id {p.id} not found')
            total += product.price * p.quantity
        return total

    def _calculate_discount(self, products, total_order, payment_method, coupon_code=None):
        if coupon_code:
            discount = self._apply_coupon(coupon_code, total_order)
            if discount:
                return discount

        if not self._has_more_than_one_product_type(products):
            discount = self.product_discount_repository.get_by_product(
                products[0].id)
            if not discount:
                return 0
            if discount.payment_method_id != payment_method:
                return 0

            if discount.mode == 'value':
                return discount.value

            return discount.value/100*total_order

        return 0

    def _apply_coupon(self, code, total_order):
        coupon = self.coupon_repository.find_by_code(code)

        if not coupon:
            return 0

        if coupon.limit == 0:
            return 0

        if coupon.mode == 'value':
            discount = coupon.value
        else:
            discount = coupon.value/100*total_order

        self.coupon_repository.use_coupon(coupon.id)

        return discount

    def _has_more_than_one_product_type(self, products: List[ProductSchema]):
        ids = set([p.id for p in products])
        return len(ids) > 1

    def _generate_code(self):
        return ''.join(random.choices(string.ascii_uppercase + string.digits, k=10))
