from fastapi.param_functions import Depends
from app.models.models import Customer, User
from app.repositories.customer_repository import CustomerRepository
from app.services.user_service import UserService
from app.api.customer.schemas import CustomerSchema, UpdateCustomerSchema, UpdateUserCustomerSchema


class CustomerService:
    def __init__(self, repository: CustomerRepository = Depends(), user_service: UserService = Depends()):
        self.repository = repository
        self.user_service = user_service

    def create_customer(self, customer: CustomerSchema):
        user = self.user_service.create_customer(User(**customer.user.dict()))
        customer_data = customer.dict()
        del customer_data['user']
        customer_model = Customer(**customer_data)
        customer_model.user_id = user.id

        return self.repository.create(customer_model)

    def update_customer(self, id, customer: UpdateCustomerSchema):
        customer_data = self._remove_none_values(customer.dict())

        return self.repository.update(id, customer_data)

    def update_user(self, id, user: UpdateUserCustomerSchema):
        user_data = self._remove_none_values(user.dict())
        return self.user_service.update_user(id, user_data)

    def _remove_none_values(self, dict: dict):
        return {key: value for (
            key, value) in dict.items() if value != None}
