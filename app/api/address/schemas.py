from pydantic import BaseModel


class AddressSchema(BaseModel):
    address: str
    city: str
    state: str
    number: str
    zipcode: str
    neighborhood: str
    primary: bool
    customer_id: int
