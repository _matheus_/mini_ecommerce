from fastapi import APIRouter, status
from fastapi.param_functions import Depends

from app.api.address.schemas import AddressSchema
from app.repositories.address_repository import AddressRepository
from app.services.address_service import AddressService

router = APIRouter()


@router.post('/', status_code=status.HTTP_201_CREATED)
def create(address: AddressSchema, service: AddressService = Depends()):
    return service.create_address(address)


@router.put('/{id}')
def update(id: int, address: AddressSchema, service: AddressService = Depends()):
    return service.update_address(id, address)


@router.delete('/{id}', status_code=status.HTTP_204_NO_CONTENT)
def remove(id: int, repository: AddressRepository = Depends()):
    repository.remove(id)
