from fastapi import APIRouter, Depends, status, HTTPException
from app.repositories.product_discount_repository import ProductDiscountRepository
from app.services.auth_service import only_admin
from app.services.product_discount_service import ProductDiscountService
from .schemas import ProductDiscountSchema
from app.common.exceptions import (
    PaymentMethodDiscountAlreadyExistsException,
    PaymentMethodsNotAvailableException
)

router = APIRouter(dependencies=[Depends(only_admin)])


@router.post('/', status_code=status.HTTP_201_CREATED)
def create(discount: ProductDiscountSchema, service: ProductDiscountService = Depends()):
    try:
        service.create_discount(discount)
    except PaymentMethodsNotAvailableException as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail=e.message)
    except PaymentMethodDiscountAlreadyExistsException as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail=e.message)


@router.put('/{id}')
def update(id: int, discount: ProductDiscountSchema, service: ProductDiscountService = Depends()):
    try:
        service.update_discount(id, discount)
    except PaymentMethodsNotAvailableException as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail=e.message)
    except PaymentMethodDiscountAlreadyExistsException as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail=e.message)


@router.get('/')
def index(repository: ProductDiscountRepository = Depends()):
    return repository.get_all()


@router.delete('/{id}', status_code=status.HTTP_204_NO_CONTENT)
def delete(id: int, repository: ProductDiscountRepository = Depends()):
    repository.remove(id)
