from typing import List
from fastapi import APIRouter, status, Depends
from app.api.supplier.schemas import ShowSupplierSchema
from app.services.auth_service import only_admin
from .schemas import PaymentMethodSchema, ShowPaymentMethodSchema
from app.models.models import PaymentMethod
from app.repositories.payment_method_repository import PaymentMethodRepository


router = APIRouter(dependencies=[Depends(only_admin)])


@router.post('/', status_code=status.HTTP_201_CREATED, response_model=ShowSupplierSchema)
def create(payment_method: PaymentMethodSchema, repository: PaymentMethodRepository = Depends()):
    return repository.create(PaymentMethod(**payment_method.dict()))


@router.get('/', response_model=List[ShowPaymentMethodSchema])
def index(repository: PaymentMethodRepository = Depends()):
    return repository.get_all()


@router.put('/{id}', response_model=ShowPaymentMethodSchema)
def update(id: int, payment_method: PaymentMethodSchema, repository: PaymentMethodRepository = Depends()):
    return repository.update(id, payment_method.dict())


@router.get('/{id}', response_model=ShowPaymentMethodSchema)
def show(id: int, repository: PaymentMethodRepository = Depends()):
    return repository.get_by_id(id)
