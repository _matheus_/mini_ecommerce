from typing import List
from fastapi import APIRouter, status
from fastapi.exceptions import HTTPException
from fastapi.param_functions import Depends

from app.services.auth_service import only_admin

from .schemas import CouponSchema, ShowCouponSchema, UpdateCouponSchema
from app.common.exceptions import CouponCodeAlreadyExistsException
from app.repositories.coupon_repository import CouponRepository
from app.services.coupon_service import CouponService

router = APIRouter(dependencies=[Depends(only_admin)])


@router.post('/', status_code=status.HTTP_201_CREATED, response_model=ShowCouponSchema)
def create(coupon: CouponSchema, service: CouponService = Depends()):
    try:
        service.create(coupon)
    except CouponCodeAlreadyExistsException as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail=e.message)


@router.get('/', response_model=List[ShowCouponSchema])
def index(repository: CouponRepository = Depends()):
    return repository.get_all()


@router.delete('/{id}', status_code=status.HTTP_204_NO_CONTENT)
def delete(id: int, repository: CouponRepository = Depends()):
    repository.remove(id)


@router.put('/{id}', response_model=ShowCouponSchema)
def update(id: int, coupon: UpdateCouponSchema, repository: CouponRepository = Depends()):
    return repository.update(id, coupon.dict())
