from typing import Optional
from pydantic import BaseModel
from datetime import datetime
from enum import Enum


class CouponMode(str, Enum):
    VALUE = 'value'
    PERCENTAGE = 'percentage'


class CouponSchema(BaseModel):
    code: str
    expire_at: Optional[datetime] = None
    limit: Optional[int] = None
    mode: CouponMode
    value: float


class UpdateCouponSchema(BaseModel):
    expire_at: datetime
    limit: int


class ShowCouponSchema(CouponSchema):
    id: int

    class Config:
        orm_mode = True
