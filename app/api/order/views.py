from fastapi import APIRouter
from fastapi.param_functions import Body, Depends
from app.repositories.order_repository import OrderRepository

from app.services.order_service import OrderService

from .schemas import OrderSchema, OrderStatusSchema
from app.services.auth_service import only_admin, only_customer

router = APIRouter()


@router.post('/')
def create(order: OrderSchema, service: OrderService = Depends(), customer=Depends(only_customer)):
    service.create_order(customer.id, order)


@router.put('/{id}', dependencies=[Depends(only_admin)])
def update_status(id: int, schema: OrderStatusSchema, repository: OrderRepository = Depends()):
    repository.set_order_status(id, schema.status)
