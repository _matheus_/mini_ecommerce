from typing import List, Optional
from pydantic import BaseModel
from enum import Enum


class OrderStatus(str, Enum):
    ORDER_PLACED = 'ORDER PLACED'
    ORDER_PAID = 'ORDER PAID'
    ORDER_SHIPPED = 'ORDER SHIPPED'
    ORDER_RECEIVED = 'ORDER RECEIVED'
    ORDER_COMPLETED = 'ORDER COMPLETED'
    ORDER_CANCELED = 'ORDER CANCELED'


class OrderStatusSchema(BaseModel):
    status: OrderStatus


class ProductSchema(BaseModel):
    id: int
    quantity: int


class OrderSchema(BaseModel):
    address_id: int
    payment_method_id: int
    coupon_code: Optional[str] = None
    products: List[ProductSchema]
