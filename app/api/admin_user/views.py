from typing import List
from fastapi import APIRouter, status
from fastapi.exceptions import HTTPException
from fastapi.param_functions import Depends
from app.api.admin_user.schemas import AdminUserSchema, ShowAdminSchema, UpadateAdminUserSchema
from app.common.exceptions import EmailAlreadyInUse
from app.models.models import User
from app.repositories.user_repository import UserRepository
from app.services.auth_service import only_admin
from app.services.user_service import UserService

router = APIRouter(dependencies=[Depends(only_admin)])


@router.post('/', status_code=status.HTTP_201_CREATED, response_model=ShowAdminSchema)
def create(user: AdminUserSchema, service: UserService = Depends()):
    try:
        return service.create_admin(User(**user.dict()))
    except EmailAlreadyInUse as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail=e.message)


@router.get('/', response_model=List[ShowAdminSchema])
def index(repository: UserRepository = Depends()):
    return repository.get_all_admins()


@router.put('/{id}', response_model=ShowAdminSchema)
def update(id: int, user: UpadateAdminUserSchema, service: UserService = Depends()):
    return service.update_admin(id, user)


@router.delete('/{id}', status_code=status.HTTP_204_NO_CONTENT)
def remove(id: int, respoitory: UserRepository = Depends()):
    respoitory.remove(id)


@router.get('/{id}', response_model=ShowAdminSchema)
def show(id: int, repository: UserRepository = Depends()):
    return repository.get_by_id(id)
