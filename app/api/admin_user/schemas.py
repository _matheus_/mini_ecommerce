from typing import Optional
from pydantic import BaseModel
from sqlalchemy import orm


class AdminUserSchema(BaseModel):
    display_name: str
    email: str
    password: str


class UpadateAdminUserSchema(BaseModel):
    display_name: Optional[str] = None
    email: Optional[str] = None
    password: Optional[str] = None


class ShowAdminSchema(BaseModel):
    display_name: str
    email: str
    id: int

    class Config:
        orm_mode = True
