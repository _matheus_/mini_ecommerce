from datetime import date
from typing import Optional
from pydantic import BaseModel


class CustomerUserSchema(BaseModel):
    display_name: str
    email: str
    password: str


class CustomerSchema(BaseModel):
    first_name: str
    last_name: str
    phone_number: str
    genre: str
    document: str
    birth_date: date
    user: CustomerUserSchema


class ShowCustomerSchema(BaseModel):
    first_name: str
    last_name: str
    phone_number: str
    genre: str
    document: str
    birth_date: date
    id: int

    class Config:
        orm_mode = True


class UpdateUserCustomerSchema(BaseModel):
    display_name: Optional[str] = None
    email: Optional[str] = None
    password: Optional[str] = None


class UpdateCustomerSchema(BaseModel):
    first_name: Optional[str] = None
    last_name: Optional[str] = None
    phone_number: Optional[str] = None
    genre: Optional[str] = None
    birth_date: Optional[str] = None
