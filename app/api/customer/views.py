from typing import List
from fastapi import APIRouter, status, HTTPException
from fastapi.param_functions import Depends
from app.repositories.customer_repository import CustomerRepository
from app.common.exceptions import EmailAlreadyInUse
from app.services.customer_service import CustomerService
from .schemas import CustomerSchema, ShowCustomerSchema, UpdateCustomerSchema
from app.services.auth_service import only_admin, only_customer

router = APIRouter()


@router.post('/', status_code=status.HTTP_201_CREATED, response_model=ShowCustomerSchema)
def create(customer: CustomerSchema, service: CustomerService = Depends()):
    try:
        return service.create_customer(customer)
    except EmailAlreadyInUse as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail=e.message)


@router.get('/', response_model=List[ShowCustomerSchema], dependencies=[Depends(only_admin)])
def index(repository: CustomerRepository = Depends()):
    return repository.get_all()


@router.put('/', response_model=ShowCustomerSchema)
def update(customer_schema: UpdateCustomerSchema, customer_model=Depends(only_customer), service: CustomerService = Depends()):
    return service.update_customer(customer_model.id, customer_schema)


@router.put('/user-info')
def update_user_info(user_schema: UpdateCustomerSchema, user=Depends(only_customer), service: CustomerService = Depends()):
    service.update_user(user.id, user_schema)
